package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaseTranslatorTest {
    @Test
    public void basicBaseTranslatorTest() {
        int[] input = {0, 1};
        int[] expectedOutput = {2, 5};
        assertArrayEquals(expectedOutput,
                          BaseTranslator.convertBase(input, 2, 10, 2));
        
        int[] in = {7, 7, 7};
        int[] expectedOut = {12, 6, 14};
        assertArrayEquals(expectedOut,
                          BaseTranslator.convertBase(in, 10, 16, 3));
        }
}
