package piwords;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WordFinderTest {
    @Test
    public void basicGetSubstringsTest() {
        String haystack = "abcde";
        String[] needles = {"ab", "abc", "de", "fg"};

        Map<String, Integer> expectedOutput = new HashMap<String, Integer>();
        expectedOutput.put("ab", 0);
        expectedOutput.put("abc", 0);
        expectedOutput.put("de", 3);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              needles));
 
        String haystack1 = "faultinourstars";
        String[] needles1 = {"tin", "ours", "tar"};

        Map<String, Integer> expectedOutput1 = new HashMap<String, Integer>();
        expectedOutput1.put("tin", 4);
        expectedOutput1.put("ours", 7);
        expectedOutput1.put("tar", 11);

        assertEquals(expectedOutput1, WordFinder.getSubstrings(haystack1,
                                                              needles1));      
    }
}
