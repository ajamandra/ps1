package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class DigitsToStringConverterTest {
    @Test
    public void basicNumberSerializerTest() {
        int[] input = {0, 1, 2, 3};
        char[] alphabet = {'d', 'c', 'b', 'a'};
        String expectedOutput = "dcba";
        assertEquals(expectedOutput,
                     DigitsToStringConverter.convertDigitsToString(
                             input, 4, alphabet));
        
        int[] in = {4, 5, 0, 1, 2, 3};
        char[] alpha = {'s', 'e', 'p', 'h', 'j', 'o'};
        String expectedOut = "joseph";
        assertEquals(expectedOut,
                     DigitsToStringConverter.convertDigitsToString(
                             in, 6, alpha));
        }

}