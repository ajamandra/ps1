package piwords;

import java.util.HashMap;
import java.util.Map;

public class WordFinder {
    /**
     * Given a String (the haystack) and an array of Strings (the needles),
     * return a Map<String, Integer>, where keys in the map correspond to
     * elements of needles that were found as substrings of haystack, and the
     * value for each key is the lowest index of haystack at which that needle
     * was found. A needle that was not found in the haystack should not be
     * returned in the output map.
     * 
     * @param haystack The string to search into.
     * @param needles The array of strings to search for. This array is not
     *                mutated.
     * @return The list of needles that were found in the haystack.
     */
    public static Map<String, Integer> getSubstrings(String haystack,
                                                     String[] needles) { 
 
    	int slen=needles.length;
        
        int [] ind= new int[slen];
        
        int i = 0;
        
        while(i < slen)
        {
        	ind[i] = haystack.indexOf(needles[i]);
        	i++;
        }
        HashMap<String, Integer> needlist = new HashMap<String, Integer>(slen);
        
        i = 0;
        
        while (i < slen)
        {
        	if(ind[i] != -1)
        	{
        		needlist.put(needles[i], ind[i]);
        	}
        	i++;
        }
        
        return needlist;
    }
}